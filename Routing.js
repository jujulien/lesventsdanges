var routing = function(app){

  var context = this;


/********************** ROUTING ****************************/
    //Redirect to documentation Index page forward
    app.get('/', function(req, res){
      res.sendFile(__dirname + '/static/index.html');
    });
}

module.exports = routing
