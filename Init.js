/********************** INITIALISATION ****************************/
var init = function() {
	
/************************* SERVER CREATION ****************************/
  var express = require('express');
  var app = express();
  //SERVER CREATION
  var server = require('http').Server(app);

  server.listen( global.configuration.serverConfig.port, global.configuration.serverConfig.ip, function() {
      console.log((new Date()) + ' Server '+global.configuration.serverConfig.ip+' is listening on port '+global.configuration.serverConfig.port);
  });

  //Express Configuration
  app.use(express.static(__dirname + '/static'));


  return {app:app,server:server}

};


module.exports = init


