
var configuration = function() {
    global.configuration = {};
    /********************* Is prod or Is server *****************************/
    global.configuration.isProd = (process.env.OPENSHIFT_NODEJS_IP ? true : false);

    /********************* Server Port and address *****************************/
    global.configuration.serverConfig = {}
    global.configuration.serverConfig.port = process.env.OPENSHIFT_NODEJS_PORT || '8080';
    global.configuration.serverConfig.ip = process.env.OPENSHIFT_NODEJS_IP || "localhost";

};


module.exports = configuration